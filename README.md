# readme
Louise's README

My name is Louise Campbell and I've been working as a UX Researcher for the last six years. Working closely with product and engineering teams by planning and implementing research techniques allowed me to inform the
design of one of the most successful Facebook products of all time, onboarding over 35,000 companies and their employees in the first year of operations.
Before working with The Companies team at Facebook I worked on research with enterprise e-commerce teams specialising in Saas ecommerce products that reach millions of customers.



I'm also the author of several articles for Invision Design Blog and UXMastery.com. I love studying and observing peoples
motivations, the psychology behind getting people to do stuff, and I thrive in structured geeky work environments.

Last but not least, in my spare time I love cooking for my family, walking around the historial streets and romantic parks in the City of London and sourcing the best dark chocolate to share with friends and colleagues!

* [Praise and portfolio](https://www.louisecampbell.co.uk)
* [LinkedIn](https://www.linkedin.com/in/louisecampbell/)
* [Twitter](https://twitter.com/@loula8_)
* [Blog](https://www.louisecampbell.co.uk/frontpage-template/blog/)
* [Invision Articles](https://www.invisionapp.com/inside-design/author/louise-campbell/)